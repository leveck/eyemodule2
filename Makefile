# Leveck, 2020

install:
	cp extracteyeimages ~/.local/bin
	cp fixeyeimages ~/.local/bin
	cp eyemodule.py ~/.local/bin
	@echo
	@echo 'If you do not have the python 2 PIL installed, copy the py files from the python2distpkg directory to ~/.local/lib/python2.[whatever python2 version you have installed].'

remove:
	rm ~/.local/bin/extracteyeimages
	rm ~/.local/bin/fixeyeimages
	rm ~/.local/bin/eyemodule.py
	@echo
	@echo 'If you copied the python 2 PIL files to ~/.local/lib/python2... you may want to manually delete these now if they are no longer needed.'
