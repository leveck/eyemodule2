# eyemodule2 photo import

Back in 2001, Tamer Fahmy wrote a python 2 program (under the GPL v2) that imported the photos from a Handspring Visor that had an eyemodule springboard module. I have modified this to work with an eyemodule2. The python2 PIL modules used by this program cannot be installed with pip at this point, so are included in this repo. It has been tested with python 2.7.12 on a debian-based distro of Linux. The original unmodified script is also present here, as it is sort of difficult to find, and may help owners of the original eyemodule.

The function that reads the images from the DB does not ATM correctly determine image size, so this is hardcoded. The 640x480 mode DOES NOT WORK - 160x120 only. Currently, the right-most portion of the image files is moved to the left-most side of the image. I wrote a bash script that calls mogrify (part of imagemagick) to fix this issue and also resize the image to 200% as they are pretty tiny by today's standards.

To install:

Make sure you have imagemagick, make, python2, and python2 PIL installed. If you do not have the python2 PIL installed, follow the directions below, as the required libraries are included in this repo. Then:

```$ make install```

This copies the scripts to ~/.local/bin. The extracteyeimages script calls eyemodule.py and fixeyeimages and is included for convenience. If you need the required python2 PIL libraries, they are included in this repo. Manually copy them to ~/.local/lib/python2.[whatever version of python2 you have], or the appropriate spot in /usr/lib that works on your system.

To uninstall:

```$ make remove```

To use:

```
$ cd palm_sync_directory
$ #whatever directory you sync your palm device to
$ extracteyeimages
$ cd ~/eyemodule/Unfiled 
$ #Unfiled is the default catagory
$ #if you use other catagories, modify the fixeyeimages script
$ ls
$ #see your images
```

## TODO 

* port to python3
* fix image size detection
  * Pretty sure the database header for the EM2 =/= the one for the EM and is the reason for this
* fix 640x480 image import

## Example

![](EM2.jpg)

Image taken with an eyemodule2 in a Handspring Visor Deluxe.
